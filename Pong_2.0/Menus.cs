﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Pong
{
    public static class Menus
    {
        static Menus()
        {

        }

        public static Menu Active_Menu = Menu.Start_Menu;

        public enum Menu
        {
            Start_Menu,
            Options_Menu,
            No_Menu
        }

        public static class Start_Menu
        {
            static Start_Menu()
            {

            }

            public static Bitmap Menu_Image = new Bitmap(Pong.Properties.Resources.Start_Menu);
            public static Bitmap One_Player_Selected = new Bitmap(Pong.Properties.Resources.Start_Menu_1_Selected);
            public static Bitmap Two_Player_Selected = new Bitmap(Pong.Properties.Resources.Start_Menu_2_Selected);

            public static List<Menu_Item> Active_Regions = new List<Menu_Item>();

            public static Menu_Item Is_Activating_Control(Point Cursor)
            {
                foreach (Menu_Item item in Active_Regions)
                {
                    if (item.Region.Contains(Cursor))
                    {
                        return item;
                    }
                }

                return null;
            }
        }
    }

    public class Menu_Item
    {
        public Delegate Action;
        public Rectangle Region;
        public Bitmap Highlight_Image;

        public class One_Player_Button : Menu_Item
        {
            public One_Player_Button(Rectangle Region, Bitmap Highlight_Image, Delegate Action)
            {
                this.Region = Region;
                this.Highlight_Image = Highlight_Image;
                this.Action = Action;
            }
        }

        public class Two_Player_Button : Menu_Item
        {
            public Two_Player_Button(Rectangle Region, Bitmap Highlight_Image, Delegate Action)
            {
                this.Region = Region;
                this.Highlight_Image = Highlight_Image;
                this.Action = Action;
            }
        }
    }
}
