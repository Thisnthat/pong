﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;

namespace Pong
{
    public class Game_State
    {
        public Game_State()
        {
            Divider_Pen.DashPattern = new float[] { 8, 16 };
        }

        public static List<Entity> Entities = new List<Entity>();

        public States State = States.Not_Running;

        public enum States
        {
            Running,
            Paused,
            Not_Running
        }

        public static Player Player_1;
        public static Player Player_2;
        public static Ball Ball;

        public Font Score_Font = new System.Drawing.Font("Arial", 12);
        public Font Message_Font = new System.Drawing.Font("Arial", 24);
        public Font FPS_Font = new System.Drawing.Font("Arial", 8);
        public Pen Divider_Pen = new Pen(Color.DimGray);

        public Brush Text_Brush = new SolidBrush(Color.Silver);
        public Brush Score_Brush = new SolidBrush(Color.White);

        public static Color Color_Key = Color.HotPink;
        public static Brush Color_Key_Brush = new SolidBrush(Color.Black);
        public static Pen Frame_Border_Color = new Pen(Color.DimGray);
        public static Brush Frame_Background_Color = new SolidBrush(Color.Black);

        public static Size Resolution = new Size(800, 600);

        public delegate void One_Second_Tick(object sender);
        public static event One_Second_Tick One_Second_Tick_Passed;

        public static void Pass_One_Second_Tick(object sender)
        {
            if (One_Second_Tick_Passed != null)
                One_Second_Tick_Passed(sender);
        }
    }
}
