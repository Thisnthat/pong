﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Pong
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;

            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);

            this.Current_Client_Size = this.ClientRectangle;

            Frame_Canvas = new Bitmap(Game_State.Resolution.Width, Game_State.Resolution.Height);
            Frame_Renderer = Graphics.FromImage(Frame_Canvas);
            Frame_Renderer.SmoothingMode = SmoothingMode.None;

            Frame_Renderer.DrawImage(Menus.Start_Menu.Menu_Image, 0, 0);

            this.BackgroundImage = Frame_Canvas;
            this.ClientSize = Game_State.Resolution;

            Current_FPS = Target_FPS;
        }

        private Rectangle Current_Client_Size;

        public Graphics Frame_Renderer;
        public Bitmap Frame_Canvas;

        private Stopwatch Frame_Timer = new Stopwatch();
        private Stopwatch Countdown_Timer = new Stopwatch();
        private Game_State Game_State = new Game_State();

        public long Tick_Cap = 0;
        public int Target_FPS = 240;
        public int Current_FPS = 0;

        public int Frames_Drawn = 0;

        private long Game_Ticks = 0;
        private long Last_Game_Ticks = 0;
        private long FPS_Ticks = 0;

        private int message_time = -1;
        private string game_message = string.Empty;

        private bool Refresh_Start_Menu = false;
        private bool Show_Mouse_Cords = false;

        public const int WM_PAINT = 0x000F;
        public const int WM_ERASEBKGND = 0x0014;

        public void Start_Game(bool Two_Player_Match)
        {
            if (Game_State.State == Game_State.States.Not_Running)
            {
                FPS_Ticks = 0;
                Game_Ticks = 0;

                Frame_Timer.Reset();
                Frame_Timer.Start();

                Game_State.Player_1 = new Player(Keys.Q, Keys.A, true);
                Game_State.Player_1.Location = new PointF(12, (Game_State.Resolution.Height - Game_State.Player_1.Size.Height) / 2);

                Game_State.Player_2 = new Player(Keys.Up, Keys.Down, Two_Player_Match);
                Game_State.Player_2.Location = new PointF((Game_State.Resolution.Width - Game_State.Player_2.Size.Width) - 12, (Game_State.Resolution.Height - Game_State.Player_2.Size.Height) / 2);

                Game_State.Ball = new Ball();

                Game_State.Entities = new List<Entity>();
                Game_State.Entities.Add(Game_State.Player_1);
                Game_State.Entities.Add(Game_State.Player_2);
                Game_State.Entities.Add(Game_State.Ball);

                Menus.Active_Menu = Menus.Menu.No_Menu;
                Game_State.State = Game_State.States.Running;

                this.MouseMove -= MainForm_MouseMove;
                this.Refresh();
                Debug.WriteLine("Game started!");
            }
        }

        public void End_Game()
        {
            Game_State.State = Game_State.States.Not_Running;
            Menus.Active_Menu = Menus.Menu.Start_Menu;
            Frame_Renderer.DrawImage(Menus.Start_Menu.Menu_Image, 0, 0);
            this.MouseMove += MainForm_MouseMove;
        }

        #region Render Methods

        private void Render_Score()
        {
            foreach (Entity ent in Game_State.Entities)
            {
                if (ent.GetType().IsAssignableFrom(typeof(Player)))
                {
                    Player p = (Player)ent;

                    string message = "Score: " + p.Score;
                    SizeF message_size = Frame_Renderer.MeasureString(message, Game_State.Message_Font);
                    int message_offset = (int)((Game_State.Resolution.Width - message_size.Width) / 4);

                    if (p.Location.X > Game_State.Resolution.Width / 2)
                    {
                        message_offset = (Game_State.Resolution.Width - message_offset) - ((int)message_size.Width / 4);
                    }

                    Frame_Renderer.DrawString(message, Game_State.Score_Font, Game_State.Score_Brush, message_offset, 12);
                }
            }
        }

        private void Render_Overlay()
        {
            Render_Score();
            Render_FPS();

            if (Game_State.Ball.Frozen)
            {
                Render_Message("Round starts in " + Game_State.Ball.Freeze_Duration);
            }

            if (message_time > 0)
            {
                Render_Message(game_message);
                message_time--;
            }
        }

        private void Render_Message(string message)
        {
            SizeF message_size = Frame_Renderer.MeasureString(message, Game_State.Message_Font);
            Frame_Renderer.DrawString(message, Game_State.Message_Font, Game_State.Text_Brush, ((Game_State.Resolution.Width - message_size.Width) / 2), ((Game_State.Resolution.Height - message_size.Height) / 4));
        }

        private void Render_Entities()
        {
            foreach (Entity e in Game_State.Entities)
            {
                Brush Entity_Brush = new SolidBrush(e.Color);
                Frame_Renderer.DrawImage(e.Draw(Entity_Brush), e.Location.X, e.Location.Y, e.Size.Width, e.Size.Height);
            }
        }

        private void Render_Divider()
        {
            Frame_Renderer.DrawLine(Game_State.Divider_Pen, new Point((Game_State.Resolution.Width / 2), 16), new Point((Game_State.Resolution.Width / 2), Game_State.Resolution.Height));
        }

        private void Render_FPS()
        {
            Frame_Renderer.DrawString("FPS:  " + Current_FPS.ToString(), Game_State.FPS_Font, Game_State.Text_Brush, ((Game_State.Resolution.Width - 64) / 2) + 4, 4);
        }

        private void Render_Complete_Frame()
        {
            Rectangle Outter = new Rectangle(0, 0, Game_State.Resolution.Width - 1, Game_State.Resolution.Height - 1);

            Frame_Renderer = Graphics.FromImage(Frame_Canvas);
            Frame_Renderer.FillRectangle(Game_State.Frame_Background_Color, Outter);

            Render_Divider();
            Render_Entities();
            Render_Overlay();

            Frame_Renderer.DrawRectangle(Game_State.Frame_Border_Color, Outter);

            int form_center_x = this.ClientSize.Width / 2;
            int form_center_y = this.ClientSize.Height / 2;
            this.Invalidate(new Rectangle(form_center_x - (Game_State.Resolution.Width / 2), form_center_y - (Game_State.Resolution.Height / 2), Game_State.Resolution.Width, Game_State.Resolution.Height));
        }

        #endregion

        #region Form Event Hooks

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (Game_State.State == Game_State.States.Running)
            {
                Game_State.Player_1.Key_Down(null, e);
                Game_State.Player_2.Key_Down(null, e);
            }

            if (e.KeyData == Keys.D1)
            {
                Start_Game(false);
            }

            if (e.KeyData == Keys.D2)
            {
                Start_Game(true);
            }

            if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                Show_Mouse_Cords = !Show_Mouse_Cords;
            }

            if (e.KeyCode == Keys.R && (Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                Game_State.Ball.Reset_Ball();
            }

            if (e.KeyCode == Keys.E && (Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                End_Game();
            }
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (Game_State.State == Game_State.States.Running)
            {
                Game_State.Player_1.Key_Up(null, e);
                Game_State.Player_2.Key_Up(null, e);
            }
        }

        private void MainForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (Menus.Active_Menu == Menus.Menu.Start_Menu)
            {
                Menu_Item active_control = Menus.Start_Menu.Is_Activating_Control(e.Location);
                if (active_control != null)
                {
                    active_control.Action.DynamicInvoke(active_control.GetType().IsAssignableFrom(typeof(Menu_Item.Two_Player_Button)));
                    toolTip_MouseCords.Dispose();
                }
            }
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            #if DEBUG
            if (ClientRectangle.Contains(e.Location) && Show_Mouse_Cords)
            {
                toolTip_MouseCords.Show(string.Format("[X={0}, Y={1}]", e.X, e.Y), this, e.Location);
            }
            else if (toolTip_MouseCords.Active)
            {
                toolTip_MouseCords.Hide(this);
            }
            #endif

            if (Menus.Active_Menu == Menus.Menu.Start_Menu)
            {
                Menu_Item active_control = Menus.Start_Menu.Is_Activating_Control(e.Location);
                if (active_control != null)
                {
                    Frame_Renderer.DrawImage(active_control.Highlight_Image, 0, 0);
                    this.Refresh();
                    Refresh_Start_Menu = true;
                }
                else if (Refresh_Start_Menu)
                {
                    Frame_Renderer.DrawImage(Menus.Start_Menu.Menu_Image, 0, 0);
                    this.Refresh();
                    Refresh_Start_Menu = false;
                }
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (Menus.Active_Menu == Menus.Menu.Start_Menu)
            {
                int center_x = this.ClientSize.Width / 2;
                int center_y = this.ClientSize.Height / 2;

                Menus.Start_Menu.Active_Regions.Clear();

                Menu_Item.One_Player_Button opb = new Menu_Item.One_Player_Button(new Rectangle(center_x - 138, center_y + 8, 270, 28), Menus.Start_Menu.One_Player_Selected, new Action<bool>(Start_Game));
                Menu_Item.Two_Player_Button tpb = new Menu_Item.Two_Player_Button(new Rectangle(center_x - 138, center_y + 46, 270, 28), Menus.Start_Menu.Two_Player_Selected, new Action<bool>(Start_Game));

                Menus.Start_Menu.Active_Regions.Add(opb);
                Menus.Start_Menu.Active_Regions.Add(tpb);
            }
        }

        #endregion

        public void Display_Message(string message, float life_in_seconds)
        {
            message_time = (int)(life_in_seconds * Target_FPS);
            this.game_message = message;
        }

        private void Update_Entities(double elapsed_ticks)
        {
            foreach (Entity e in Game_State.Entities)
            {
                e.Update(elapsed_ticks);
            }
        }

        private void Cap_Frame_Rate(long tick_difference)
        {
            int target_fps_miliseconds = (int)Math.Floor(1000.0 / (Target_FPS + 4) / 2);

            if (Frames_Drawn % 2 == 0 && tick_difference < target_fps_miliseconds)
            {
                Thread.Sleep((target_fps_miliseconds - (int)tick_difference));
            }
        }

        private void Update_FPS()
        {
            Current_FPS = Frames_Drawn;
            Frames_Drawn = 0;

            FPS_Ticks = 0;
        }

        private void Track_Game_Ticks()
        {
            Game_Ticks = Frame_Timer.ElapsedMilliseconds;

            if (FPS_Ticks == 0)
            {
                FPS_Ticks = Game_Ticks;
            }
        }

        private void Game_Loop()
        {
            while (Game_State.State == Game_State.States.Running || Game_State.State == Game_State.States.Paused)
            {
                if (Game_State.State == Game_State.States.Running)
                {
                    Perform_Game_Render();
                }
            }
        }

        private void Perform_Game_Render()
        {
            Track_Game_Ticks();
            Tick_Cap = (1000 / Target_FPS + Game_Ticks);

            double elapsed_ticks = Game_Ticks - Last_Game_Ticks;

            Frames_Drawn++;
            Update_Entities(elapsed_ticks);

            if ((Frame_Timer.ElapsedMilliseconds - FPS_Ticks) >= 1000)
            {
                Game_State.Pass_One_Second_Tick(this);
                this.Text = "Pong - FPS: " + Current_FPS.ToString();
                Update_FPS();
            }

            Last_Game_Ticks = Game_Ticks;

            lock (Frame_Canvas)
            {
                Render_Complete_Frame();
                this.Invalidate();
            }

            Cap_Frame_Rate(Frame_Timer.ElapsedMilliseconds - Last_Game_Ticks);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_ERASEBKGND)
            {
                m.Result = new IntPtr(0);
                return;
            }

            if (m.Msg == WM_PAINT)
            {
                base.WndProc(ref m);

                if (Game_State.State == Game_State.States.Running)
                {
                    Perform_Game_Render();
                }
            }
            else
            {
                base.WndProc(ref m);
            }
        }
    }
}
