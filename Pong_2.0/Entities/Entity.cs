﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

namespace Pong
{
    public class Entity
    {
        public Entity()
        {

        }

        public enum Entity_Types
        {
            Player,
            Ball,
            Other
        }

        public Entity_Types Type;

        public PointF Speed = PointF.Empty;
        public PointF Location = PointF.Empty;
        public SizeF Size = SizeF.Empty;

        public Color Color = Color.White;

        public static int Volocity_Constant = 420;

        public bool Has_Collision = true;
        public bool Show_Collision = false;

        public Bitmap Sprite;
        public Graphics Render;

        public virtual RectangleF Collision_Mask()
        {
            return new RectangleF(this.Location.X, this.Location.Y, this.Size.Width, this.Size.Height);
        }

        public bool Has_Collided(Entity other_entity)
        {
            RectangleF my_mask = Collision_Mask();
            RectangleF secondary_mask = other_entity.Collision_Mask();

            return !(my_mask.X > secondary_mask.X + secondary_mask.Width
                  || my_mask.X + my_mask.Width < secondary_mask.X
                  || my_mask.Y > secondary_mask.Y + secondary_mask.Height
                  || my_mask.Y + my_mask.Height < secondary_mask.Y);
        }

        public Bitmap Draw_Collision(Color color)
        {
            if (Sprite == null)
            {
                throw new NullReferenceException();
            }

            int border_size = 1;

            Bitmap Collision = (Bitmap)Sprite.Clone();

            RectangleF Collision_Rectangle = Collision_Mask();

            Render = Graphics.FromImage(Collision);
            Render.FillRectangle(new SolidBrush(color), Collision_Rectangle);
            Render.FillRectangle(new SolidBrush(Color.HotPink), Collision_Rectangle.X + border_size, Collision_Rectangle.Y + border_size, Collision_Rectangle.Width - border_size, Collision_Rectangle.Height - border_size);

            ImageAttributes attr = new ImageAttributes();
            attr.SetColorKey(Color.HotPink, Color.HotPink);

            Rectangle dest_rectangle = new Rectangle(0, 0, Collision.Width, Collision.Height);
            Render.DrawImage(Collision, dest_rectangle, 0, 0, Collision.Width, Collision.Height, GraphicsUnit.Pixel, attr);

            return Collision;
        }

        public virtual Bitmap Draw(Brush brush)
        {
            Render = Graphics.FromImage(Sprite);
            Render.FillRectangle(brush, 0, 0, Size.Width, Size.Height);

            return Sprite;
        }

        public virtual void Update(double elapsed_ticks)
        {
            Location.X += (Speed.X * (float)elapsed_ticks / 1000);
            Location.Y += (Speed.Y * (float)elapsed_ticks / 1000);
        }
    }
}
