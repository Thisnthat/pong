﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace Pong
{
    public class Ball : Entity
    {
        public Ball()
        {
            this.Size = new Size(12, 12);
            this.Sprite = new Bitmap((int)this.Size.Width, (int)this.Size.Height);

            Render = Graphics.FromImage(this.Sprite);

            Reset_Ball();
        }

        private Random rand = new Random();
        public PointF Volocity = PointF.Empty;
        public Player Last_Player_Hit;

        public PointF Last_Location = PointF.Empty;

        private Pen border = new Pen(Color.DarkRed);

        public int Freeze_Duration = 0;
        public bool Frozen
        {
            get
            {
                return (Freeze_Duration > 0);
            }
        }

        //Generates a random non zero speed for the ball
        public Point Generate_Random_Speed()
        {
            bool negative_x_speed = (rand.Next(0, 100000) % 2 == 0);
            bool negative_y_speed = (rand.Next(0, 100000) % 2 == 0);

            Point new_speed = new Point(rand.Next(1, 4), rand.Next(1, 4));

            if (negative_x_speed)
            {
                new_speed.X = -new_speed.X;
            }

            if (negative_y_speed)
            {
                new_speed.Y = -new_speed.Y;
            }

            return new_speed;
        }

        public override void Update(double elapsed_ticks)
        {
            if (Volocity.X == 0 || Volocity.Y == 0)
            {
                return;
            }

            Speed.X = (Volocity.X * ((Volocity_Constant - 50) / Math.Abs(Volocity.X)));
            Speed.Y = (Volocity.Y * ((Volocity_Constant - 50) / Math.Abs(Volocity.Y)));

            base.Update(elapsed_ticks);

            if (this.Location == this.Last_Location)
            {
                return;
            }

            this.Last_Location = this.Location;

            if (this.Location.Y >= (Game_State.Resolution.Height - this.Size.Height))
            {
                Volocity.Y = (Volocity.Y * -1);
                this.Location.Y = Game_State.Resolution.Height - this.Size.Height;
            }

            if (this.Location.Y <= 0)
            {
                Volocity.Y = (Volocity.Y * -1);
                this.Location.Y = 0;
            }

            if (this.Location.X >= (Game_State.Resolution.Width - this.Size.Width))
            {
                Game_State.Player_1.Score++;
                Reset_Ball();
            }

            if (this.Location.X <= 0)
            {
                Game_State.Player_2.Score++;
                Reset_Ball();
            }

            foreach (Entity e in Game_State.Entities)
            {
                if ((e.GetType().IsAssignableFrom(typeof(Player))))
                {
                    bool hit_player = Has_Collided(e);
                    if (hit_player)
                    {
                        Last_Player_Hit = (Player)e;
                        RectangleF entity_collision_mask = e.Collision_Mask();

                        bool ball_is_positive = (this.Volocity.X > 0);

                        if (ball_is_positive)
                        {
                            if (entity_collision_mask.X > (Game_State.Resolution.Width / 2))
                            {
                                if (this.Location.X + this.Size.Width > entity_collision_mask.X + 4)
                                {
                                    Volocity.Y = Volocity.Y * -1;
                                    Volocity.X = (Volocity.Y * -1) - 1;

                                    if (Volocity.X == 0)
                                    {
                                        Volocity.X--;
                                    }

                                    if (Volocity.Y == 0)
                                    {
                                        Volocity.Y--;
                                    }

                                    Debug.WriteLine("X: " + Volocity.X.ToString());
                                    Debug.WriteLine("Y: " + Volocity.Y.ToString());
                                }
                                else
                                {
                                    Volocity.X = Volocity.X * -1;
                                }
                            }
                        }
                        else
                        {
                            if (entity_collision_mask.X < (Game_State.Resolution.Width / 2))
                            {
                                if (this.Location.X < entity_collision_mask.X - 4)
                                {
                                    Volocity.Y = Volocity.Y * -1;
                                }
                                else
                                {
                                    Volocity.X = Volocity.X * -1;
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }

        public override Bitmap Draw(Brush brush)
        {
            Bitmap temp = base.Draw(brush);
            Render = Graphics.FromImage(temp);
            Render.DrawRectangle(border, 0, 0, this.Size.Width - 1, this.Size.Height - 1);

            return temp;
        }

        public void Reset_Ball()
        {
            this.Location = new PointF((Game_State.Resolution.Width - this.Size.Width) / 2, (Game_State.Resolution.Height - this.Size.Height) / 2);
            Freeze(2);
        }

        public void Freeze(int seconds)
        {
            this.Volocity = new PointF(0, 0);

            Freeze_Duration = seconds;
            Game_State.One_Second_Tick_Passed += Game_State_One_Second_Tick_Passed;
        }

        public void Unfreeze()
        {
            this.Volocity = Generate_Random_Speed();
        }

        private void Game_State_One_Second_Tick_Passed(object sender)
        {
            if (Freeze_Duration <= 0)
            {
                Game_State.One_Second_Tick_Passed -= Game_State_One_Second_Tick_Passed;
                Unfreeze();
            }
            else
            {
                Freeze_Duration--;
            }
        }
    }
}
