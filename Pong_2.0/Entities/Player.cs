﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Pong
{
    public class Player : Entity_Controlled
    {
        public Player(Keys up_key, Keys down_key, bool is_human)
        {
            this.Up_Key = up_key;
            this.Down_Key = down_key;

            this.Is_Human = is_human;

            this.Size = new Size(12, 80);
            this.Sprite = new Bitmap((int)Size.Width, (int)Size.Height);

            Render = Graphics.FromImage(this.Sprite);
        }

        public Keys Up_Key;
        public Keys Down_Key;

        private bool Is_Up_Key_Pressed = false;
        private bool Is_Down_Key_Pressed = false;

        private Random rand = new Random();

        public bool Is_Human = false;

        public int Score = 0;

        public override void Key_Up(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Down_Key)
            {
                Is_Down_Key_Pressed = false;
            }

            if (e.KeyData == Up_Key)
            {
                Is_Up_Key_Pressed = false;
            }
        }

        public override void Key_Down(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Down_Key)
            {
                Is_Down_Key_Pressed = true;
            }

            if (e.KeyData == Up_Key)
            {
                Is_Up_Key_Pressed = true;
            }
        }

        public override void Update(double elapsed_ticks)
        {
            if (Is_Human)
            {
                Human_Move();
            }
            else
            {
                Computer_Move();
            }

            base.Update(elapsed_ticks);

            if (this.Location.Y > (Game_State.Resolution.Height - this.Size.Height))
            {
                this.Location.Y = (Game_State.Resolution.Height - this.Size.Height);
            }

            if (this.Location.Y < 0)
            {
                this.Location.Y = 0;
            }
        }

        public void Human_Move()
        {
            Speed.Y = 0;

            if (Is_Up_Key_Pressed)
            {
                Speed.Y += -Volocity_Constant;
            }

            if (Is_Down_Key_Pressed)
            {
                Speed.Y += Volocity_Constant;
            }
        }

        public void Computer_Move()
        {
            int active_range = 325;
            int min_variance = 56;
            int max_variance = 92;

            int center_frame = (int)((Game_State.Resolution.Height / 2) - (this.Size.Height / 2));

            Speed.Y = 0;

            if ((Game_State.Ball.Frozen && Game_State.Ball.Freeze_Duration < 2) && (this.Location.Y < center_frame - 4 || this.Location.Y > center_frame + 4))
            {
                if (this.Location.Y > center_frame)
                {
                    Speed.Y += -Volocity_Constant;
                }
                else
                {
                    Speed.Y += Volocity_Constant;
                }
                return;
            }

            if (Game_State.Ball.Volocity.X < 0)
            {
                return;
            }

            if (this.Location.X - Game_State.Ball.Location.X < active_range)
            {
                if (rand.Next(0, 40) == 5)
                {
                    return;
                }

                int perfect_y_location = (int)(Game_State.Ball.Location.Y - (Game_State.Ball.Size.Height / 2));

                if (this.Location.Y + (this.Size.Height / 2) > perfect_y_location)
                {
                    Speed.Y += (-Volocity_Constant + (rand.Next(min_variance, max_variance)));
                }

                if (this.Location.Y + (this.Size.Height / 2) < perfect_y_location)
                {
                    Speed.Y += (Volocity_Constant - (rand.Next(min_variance, max_variance)));
                }
            }
        }
    }
}
